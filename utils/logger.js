const dateFormatter = require('./date_formatter');

const colors = {
    FG_RED: '\x1b[31m',
    FG_GREEN: '\x1b[32m',
    FG_CYAN: '\x1b[36m',
    FG_WHITE: '\x1b[37m',
    FG_BLUE: '\x1b[34m',
}

const logger = {
    info(...text) {
        console.log(
            `${colors.FG_WHITE}%s${colors.FG_GREEN}%s${colors.FG_WHITE}`,
            `[${dateFormatter(new Date(), 'YY/MM/DD hh:mm:ss')}] `,
             ...text
        );
    },
    log(...text) {
        console.log(
            `${colors.FG_WHITE}%s${colors.FG_CYAN}%s${colors.FG_WHITE}`,
            `[${dateFormatter(new Date(), 'YY/MM/DD hh:mm:ss')}] `,
             ...text
        );
    },
    error(...text) {
        console.log(
            `${colors.FG_WHITE}%s${colors.FG_RED}%s${colors.FG_WHITE}`,
            `[${dateFormatter(new Date(), 'YY/MM/DD hh:mm:ss')}] `,
             ...text
        );
    },
    question(...text) {
        console.log(
            `${colors.FG_WHITE}%s${colors.FG_BLUE}%s${colors.FG_WHITE}`,
            `[${dateFormatter(new Date(), 'YY/MM/DD hh:mm:ss')}] `,
             ...text
        );
    },
};

module.exports = logger;
