const util = require('util');
const exec = util.promisify(require('child_process').exec);

const logger = require('./logger');

const formatVersion = (major, minor, patch) => ({
    major,
    minor,
    patch,
});

async function isDockerInstalled() {
    logger.info('Check if docker is installed');
    try {
        const { stdout, stderr } = await exec('docker --version');
        if (stdout) {
            logger.log('Found version:', stdout.replace(/[^.0-9]/g,''));
            return true;
        } else if (stderr) {
            logger.error('Failed to execute the check command. Whoops :(');
            return false;
        }
    } catch(error) {
        logger.error('docker is missing from this machine');
        return false;
    }
};

async function isDockerComposeInstalled() {
    logger.info('Check if docker-compose is installed');
    try {
        const { stdout, stderr } = await exec('docker-compose --version');
        if (stdout) {
            logger.log('Found version:', stdout.replace(/[^.0-9]/g,''));
            return true;
        } else if (stderr) {
            logger.error('Failed to execute the check command. Whoops :(');
            return false;
        }
    } catch(error) {
        logger.error('docker-compose is missing from this machine');
        return false;
    }
};

async function isGitInstalled() {
    logger.info('Check if git is installed');
    try {
        const { stdout, stderr } = await exec('git --version');
        if (stdout) {
            logger.log('Found version:', stdout.replace(/[^.0-9]/g,''));
            return true;
        } else if (stderr) {
            logger.error('Failed to execute the check command. Whoops :(');
            return false;
        }
    } catch(error) {
        logger.error('git is missing from this machine');
        return false;
    }
};

async function getNodeJSVersion() {
    logger.info('Get Node.JS version');
    try {
        const { stdout, stderr } = await exec('node --version');
        if (stdout) {
            const cleanVersion = stdout.replace(/[^.0-9]/g,'');
            const versionAsArray = cleanVersion.split('.');
            logger.log('Found version:', cleanVersion);
            return formatVersion(...versionAsArray);
        } else if (stderr) {
            logger.error('Failed to execute the check command. Whoops :(');
            return formatVersion(0,0,0);
        }
    } catch(error) {
        logger.error('something went terrible wrong...');
        return formatVersion(0,0,0);
    }
};

module.exports = {
    isDockerInstalled,
    isDockerComposeInstalled,
    isGitInstalled,
    getNodeJSVersion,
}
