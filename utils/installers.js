const util = require('util');
const exec = util.promisify(require('child_process').exec);

const logger = require('./logger');

async function executeCommand(command, { ignoreStderr = false }) {
    logger.info('Running:', command);
    try {
        const { stdout, stderr } = await exec(command);
        if (stdout || ignoreStderr) {
            logger.log('Command was executed successfully');
            return true;
        } else if (stderr) {
            logger.error('Failed to execute the command. Whoops :(');
            logger.error(stderr);
            return false;
        }
    } catch(error) {
        logger.error('something is very wrong...');
        logger.error(error);
        return false;
    }
};

const installNodeUsingNpm = async () => {
    const response = await executeCommand('npm install npm@latest -g')
    if (response) {
        logger.info('Successfully installed latest Node.JS');
    } else {
        logger.error('Failed to install latest Node.JS');
    }
}

const installChocolatey = async () => {
    const response = await executeCommand('cinst');
    if (!response) {
        const command = `@"%SystemRoot%\System32\WindowsPowerShell\v1.0\powershell.exe" -NoProfile -InputFormat None -ExecutionPolicy Bypass -Command "iex ((New-Object System.Net.WebClient).DownloadString('https://chocolatey.org/install.ps1'))" && SET "PATH=%PATH%;%ALLUSERSPROFILE%\chocolatey\bin"`;
        await executeCommand(command)
    }
};

const installBrew = async () => {
    const response = await executeCommand('brew --version');
    if (!response) {
        const command = `ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"`;
        await executeCommand(command)
    }
};

const chainCommands = async (commands) => {
    for (let i = 0; i < commands.length; i++) {
        const response = await executeCommand(commands[i]);
        if (!response) {
            return false;
        }       
    }
    return true;
};

const installNode = {
    async darwin() {
        const command = `curl "https://nodejs.org/dist/latest/node-${'${'}VERSION:-$(wget -qO- https://nodejs.org/dist/latest/ | sed -nE 's|.*>node-(.*)\\.pkg</a>.*|\\1|p')}.pkg" > "$HOME/Downloads/node-latest.pkg" && sudo installer -store -pkg "$HOME/Downloads/node-latest.pkg" -target "/"`;
        const response = await executeCommand(command);
        if (response) {
            logger.info('Successfully installed latest Node.JS');
        } else {
            logger.error('Failed to install latest Node.JS');
        }
    },
    async win32() {
        await installChocolatey();
        const response_1 = await executeCommand('cinst nodejs.install');
        
        if (response) {
            logger.info('Successfully installed latest Node.JS');
        } else {
            logger.error('Failed to install latest Node.JS');
        }
    },
    async linux() {
        const response = await chainCommands(
            'curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -',
            'sudo apt-get install -y nodejs'
        );
        if (response) {
            logger.info('Successfully installed latest Node.JS');
        } else {
            logger.error('Failed to install latest Node.JS');
        }
    },
}

const installGit = {
    async darwin() {
        await installBrew();
        const response = await executeCommand('brew install git');
        if (response) {
            logger.info('Successfully installed latest GIT');
        } else {
            logger.error('Failed to install latest GIT');
            process.exit(1);
        }
    },
    async win32() {
        await installChocolatey();
        const response = await executeCommand('cinst git.install');
        
        if (response) {
            logger.info('Successfully installed latest GIT');
        } else {
            logger.error('Failed to install latest GIT');
            process.exit(1);
        }
    },
    async linux() {
        const response = await chainCommands(
            'sudo apt-get update',
            'sudo apt-get install -y git-all',
        );
        if (response) {
            logger.info('Successfully installed latest GIT');
        } else {
            logger.error('Failed to install latest GIT');
            process.exit(1);
        }
    },
}

const installDocker = {
    async darwin() {
        const response = await executeCommand('open -a Safari https://docs.docker.com/docker-for-mac/install/');
        if (response) {
            logger.info('Successfully open Docker download page to manually install docker, after you\'re done, restart the script.');
        } else {
            logger.error('Failed to open Safari');
            process.exit(1);
        }
    },
    async win32() {
        await installChocolatey();
        const response = await executeCommand('choco install docker-for-windows');
        
        if (response) {
            logger.info('Successfully installed latest docker-for-windows');
        } else {
            logger.error('Failed to install latest docker-for-windows');
            process.exit(1);
        }
    },
    async linux() {
        const response = await chainCommands(
            'sudo apt-get update',
            'sudo apt-get install apt-transport-https ca-certificates curl software-properties-common',
            'curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -',
            'sudo apt-key fingerprint 0EBFCD88',
            'sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"',
            'sudo apt-get update',
            'sudo apt-get install docker-ce',
            'sudo groupadd docker',
            'sudo usermod -aG docker $USER',
        );
        if (response) {
            logger.info('Successfully installed latest docker version, to work without sudo, logout and login');
        } else {
            logger.error('Failed to install latest docker');
            process.exit(1);
        }
    },
}

const installDockerCompose = {
    async darwin() {
        const response = await executeCommand('open -a Safari https://docs.docker.com/docker-for-mac/install/');
        if (response) {
            logger.info('Successfully open Docker download page to manually install docker, after you\'re done, restart the script.');
        } else {
            logger.error('Failed to open Safari');
            process.exit(1);
        }
    },
    async win32() {
        await installChocolatey();
        const response = await executeCommand('choco install docker-for-windows');
        
        if (response) {
            logger.info('Successfully installed latest docker-for-windows');
        } else {
            logger.error('Failed to install latest docker-for-windows');
            process.exit(1);
        }
    },
    async linux() {
        const response = await chainCommands(
            'sudo -i',
            'curl -L https://github.com/docker/compose/releases/download/1.16.0/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose',
            'chmod +x /usr/local/bin/docker-compose',
        );
        if (response) {
            logger.info('Successfully installed latest docker-compose');
        } else {
            logger.error('Failed to install latest docker-compose');
            process.exit(1);
        }
    },
}

module.exports = {
    installNode,
    installGit,
    installDocker,
    installDockerCompose,
    executeCommand,
    chainCommands,
}
