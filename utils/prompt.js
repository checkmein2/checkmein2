const readline = require('readline');

const logger = require('./logger');

const prompt = (question) => new Promise((resolve) => {
    const rl = readline.createInterface({
        input: process.stdin,
        output: process.stdout
    });     

    logger.question(question);
    rl.question('', (answer) => {
        let response = false;
        if (answer.toLowerCase() === 'y') {
            response = true;
        }
        rl.close();
        resolve({ response });
    });
    
})

module.exports = prompt;
