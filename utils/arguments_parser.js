const args = process.argv.slice(2);
const supportedArguments = ['p', 'path'];

const mapShortArgToLong = {
    p: 'path',
};

const formattedArguments = args.reduce((acc, value) => {
    const { args, property } = acc;
    if (value.startsWith('-')) {
        const cleanValue = value.replace(/-/g, '');
        const newProperty = mapShortArgToLong[cleanValue] || cleanValue;
        return Object.assign(
            acc,
            {
                args: Object.assign({}, args, { [newProperty]: true }),
                property: newProperty
            }
        )
    }
    return Object.assign(
        acc,
        {
            args: Object.assign({}, args, { [property]: value }),
            property: '_'
        }
    )        
}, { args: {}, property: '_' })

module.exports = {
    supportedArguments,
    args: formattedArguments.args
};
