const dayOfTheWeekLong = [
    'Sunday',
    'Monday',
    'Tuesday',
    'Wednesday',
    'Thursday',
    'Friday',
    'Saturday'
  ];
  
  const dayOfTheWeekShort = [
    'Sun',
    'Mon',
    'Tue',
    'Wed',
    'Thu',
    'Fri',
    'Sat'
  ];
  
  const monthLong = [
    'January',
    'February',
    'March',
    'April',
    'May',
    'June',
    'July',
    'August',
    'September',
    'October',
    'November',
    'December'
  ];
  
  const monthShort = [
    'Jan',
    'Feb',
    'Mar',
    'Apr',
    'May',
    'Jun',
    'Jul',
    'Aug',
    'Sep',
    'Oct',
    'Nov',
    'Dec'
  ];
  
  const dateFormatter = (value, format = 'MM/DD/YY hh:mm:ss TZ') => {
    const actions = {
      MM: date => date.getMonth() + 1,
      month: date => monthLong[date.getMonth()],
      mo: date => monthShort[date.getMonth()],
      DD: date => date.getDate(),
      day: date => dayOfTheWeekLong[date.getDay()],
      da: date => dayOfTheWeekShort[date.getDay()],
      YY: date => date.getFullYear(),
      hh: date => date.getHours(),
      mm: date => (date.getMinutes() < 10 ? `0${date.getMinutes()}` : date.getMinutes()),
      ss: date => (date.getSeconds() < 10 ? `0${date.getSeconds()}` : date.getSeconds()),
      TZ: date => (`GMT${date.getTimezoneOffset() / 60}`)
    };
    const split = format.match(/[A-z]+/g);
    const date = new Date(value);
  
    return split.reduce((formatedDate, type) =>
        formatedDate.replace(new RegExp(type, 'g'), actions[type] ? actions[type](date) : type),
      format);
  };
  
  const convertToMs = ({ amount, unit }) => {
    switch (unit) {
      case 'h':
        return amount * 3600000;
      case 'min':
        return amount * 60000;
      case 's':
        return amount * 1000;
      default:
        return amount;
    }
  };
  
  module.exports = dateFormatter;
  