const fs = require('fs');
const { executeCommand } = require('./installers');
const prompt = require('./prompt');

async function gitClone({ path, repo, name }) {
    return new Promise(async resolve => {
        const response = await executeCommand(`git clone --progress -q ${repo} ${path !== '' ? `${path}/${name}` : name}`, { ignoreStderr: true });
        resolve(response);    
    })
}

const chainCommands = async (commands) => {
    const responsesInOrder = [];
    for (let i = 0; i < commands.length; i++) {
        const response = await gitClone(commands[i]);
        responsesInOrder.push(response);
    }
    return responsesInOrder;
};

const repositories = [
    {
        name: 'checkmein2',
        repo: 'git@gitlab.com:checkmein2/checkmein2.git',
    },
    {
        name: 'checkmein2-ui',
        repo: 'git@gitlab.com:checkmein2/checkmein2-ui.git',
    },
    {
        name: 'checkmein2-campaign-service',
        repo: 'git@gitlab.com:checkmein2/checkmein2-campaign-service.git',
    },
    {
        name: 'checkmein2-ws',
        repo: 'git@gitlab.com:checkmein2/checkmein2-ws.git',
    },
    {
        name: 'checkmein2-api-gateway',
        repo: 'git@gitlab.com:checkmein2/checkmein2-api-gateway.git',
    },
    {
        name: 'checkmein2-media-service',
        repo: 'git@gitlab.com:checkmein2/checkmein2-media-service.git',
    },
    {
        name: 'checkmein2-notification-service',
        repo: 'git@gitlab.com:checkmein2/checkmein2-notification-service.git',
    },
    {
        name: 'checkmein2-user-management-service',
        repo: 'git@gitlab.com:checkmein2/checkmein2-user-management-service.git',
    },
    {
        name: 'checkmein2-mobile',
        repo: 'git@gitlab.com:checkmein2/checkmein2-mobile.git',
    },
    {
        name: 'checkmein2-devops',
        repo: 'git@gitlab.com:checkmein2/checkmein2-devops.git',
    },
    {
        name: 'checkmein2-activity-service',
        repo: 'git@gitlab.com:checkmein2/checkmein2-activity-service.git',
    },
];

const handleResponses = (responses) => {
    const failed = responses.reduce((acc, response, index) => {
        if (!response) {
            acc.push(repositories[index].name)
        }
        return acc;
    }, [])

    if (failed.length > 0) {
        return {
            response: false,
            failed: failed.join(','),
        };
    }
    return {
        response: true,
    }
}

function cloneProjectWithoutSSHPassword({ path = '' }) {
    return new Promise((resolve) => {
        const promises = repositories.map(({ name, repo }) => gitClone({ path, name, repo }));

        Promise.all(promises).then(responses => { resolve(handleResponses(responses)); });
    })
}

async function cloneProjectWithSSHPassword({ path = ''}) {
    const commands = repositories.map(({ name, repo }) => ({ path, name, repo }));
    const responses = await chainCommands(commands);

    return handleResponses(responses);
}

async function cloneProject(...arg) {
    const { response } = await prompt('Do you have a passphrase on your ssh key ? (y/n): ')
    if (response) {
        return await cloneProjectWithSSHPassword(...arg);
    }

    return await cloneProjectWithoutSSHPassword(...arg);
}
module.exports = cloneProject;