const util = require('util');
const exec = util.promisify(require('child_process').exec);
const os = require('os');

const prompt = require('./utils/prompt');
const logger = require('./utils/logger');
const {
    getNodeJSVersion,
    isGitInstalled,
    isDockerInstalled,
    isDockerComposeInstalled,
} = require('./utils/dependencies_checker');
const {
    installNode,
    installGit,
    installDocker,
    installDockerCompose,
} = require('./utils/installers');

const {
    args,
    supportedArguments,
} = require('./utils/arguments_parser');

const cloneProject = require('./utils/git_clone');

async function initiateDeploy() {
    logger.info('Initiate checkmein development deploy')
    const platform = os.platform();
    
    const nodeVersion = await getNodeJSVersion();
    if (nodeVersion.major < 8) {
        logger.error('Your Node.js is less then 8, this might cause issues with the app');
        const { response } = await prompt('Would you like for me to install latest Node.js version ? (y/n): ')
        if (response) {
            await installNode[platform]();
        }
    }
    
    const isGit = await isGitInstalled();
    if (!isGit) {
        logger.info('Installing GIT');
        await installGit[platform]();
    }
    
    const isDocker = await isDockerInstalled();
    if (!isDocker) {
        logger.info('Installing Docker');
        await installDocker[platform]();
    }
    
    const isDockerCompose = await isDockerComposeInstalled();
    if (!isDockerCompose) {
        logger.info('Installing Docker');
        await installDockerCompose[platform]();        
    }

    if ((args.path && typeof args.path === 'string') || typeof args.path === 'undefined') {
        logger.info(`Clonning projects here: ${args.path || __dirname}`);
        const { response, failed } = await cloneProject({ path: args.path });
        if (response) {
            logger.info('Successfully cloned project');
        } else {
            logger.error(`Failed to clone the following repositories: ${failed}`);
        }
    }
}

initiateDeploy();